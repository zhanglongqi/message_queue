
CFLAGS = -g -lrt
CC = gcc

.PHONY: all
all:  exp1 exp2

exp1: server client

server: common.h server.c
	$(CC) -o server server.c $(CFLAGS)

client: common.h client.c
	$(CC) -o client client.c $(CFLAGS)


exp2: server2 client2

server2: server2.c
	$(CC) -o server2 server2.c $(CFLAGS)

client2: client2.c
	$(CC) -o client2 client2.c $(CFLAGS)

.PHONY: clean
clean:
	rm -fv client server client2 server2
	rm -fv *.o
